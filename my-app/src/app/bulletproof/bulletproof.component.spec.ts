import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulletproofComponent } from './bulletproof.component';

describe('BulletproofComponent', () => {
  let component: BulletproofComponent;
  let fixture: ComponentFixture<BulletproofComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulletproofComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletproofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
