import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutUsComponent } from './about-us/about-us.component';
import { BulletproofComponent } from './bulletproof/bulletproof.component';
import {RulesComponent} from './rules/rules.component';


const routes: Routes = [
  { path: '', redirectTo: '/bulletproof', pathMatch: 'full' },
  { path: 'bulletproof', component: BulletproofComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'rules', component: RulesComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
